from math import ceil
from copy import deepcopy
from random import randint
from functools import reduce
import re


# Lists


def bubble_sort(lst):
    """
    A simple Bubble sort algorithm.
    >>> bubble_sort([54, 26, 91, 17, 77, 31, 44, 55, 20])
    [17, 20, 26, 31, 44, 54, 55, 77, 91]
    """
    for passnum in range(len(lst) - 1, 0, -1):
        for i in range(passnum):
            if lst[i] > lst[i + 1]:
                temp = lst[i]
                lst[i] = lst[i + 1]
                lst[i + 1] = temp
    return lst


def chunk(lst, size):
    """
    Uses range to create a list of desired size. Then use map on this list and fill it with splices of lst.
    >>> chunk([1, 2, 3, 4, 5], 2)
    [[1, 2], [3, 4], [5]]
    """
    return list(
        map(lambda x: lst[x * size:x * size + size],
            list(range(0, ceil(len(lst) / size)))))


def compact(lst):
    """
    Use filter() to filter out falsey values (False, None, 0, and "").
    >>> compact([0, 1, False, 2, '', 3, 'a', 's', 34])
    [1, 2, 3, 'a', 's', 34]
    """
    return list(filter(bool, lst))


def count_by(arr, fn=lambda x: x):
    """
    Groups the elements of a list based on the given function and returns the count of elements in each group.

    Use map() to map the values of the list using the given function. Iterate over the map and increase the the elements
    count each time it occurs.

    >>> count_by(['one', 'two', 'three'], len)
    {3: 2, 5: 1}
    >>> from math import floor
    >>> count_by([6.1, 4.2, 6.3], floor)
    {6: 2, 4: 1}

    note:: Already implemented via collections.Counter
    """
    key = {}
    for el in map(fn, arr):
        key[el] = 0 if el not in key else key[el]
        key[el] += 1
    return key


def count_occurrences(lst, val):
    """
    Counts the occurrences of a value in an list.

    Uses the list comprehension to increment a counter each time you encounter the specific value inside the list.

    >>> count_occurrences([1, 1, 2, 1, 2, 3], 1)
    3

    note:: Already implemented via list.count().
    """
    return len([x for x in lst if x == val and type(x) == type(val)])


def spread(arg):
    """
    Implements javascript's [].concat(...arr). Flattens the list(non-deep) and returns an list.
    >>> spread([1,2,3,[4,5,6],[7],8,9])
    [1, 2, 3, 4, 5, 6, 7, 8, 9]
    """
    ret = []
    for i in arg:
        if isinstance(i, list):
            ret.extend(i)
        else:
            ret.append(i)
    return ret


def deep_flatten(lst):
    """
    Deep flattens a list.

    Use recursion. Use list.extend() with an empty list (result) and the spread function to flatten a list. Recursively
    flatten each element that is a list.

    >>> deep_flatten([1, [2], [[3], 4], 5])
    [1, 2, 3, 4, 5]
    """
    result = []
    result.extend(
        spread(list(map(lambda x: deep_flatten(x) if type(x) == list else x, lst))))
    return result


def difference(a, b):
    """
    Returns the difference between two iterables.

    Use list comprehension to only keep values not contained in b

    >>> difference([1, 2, 3], [1, 2, 4])
    [3]
    """
    return [item for item in a if item not in b]


def difference_by(a, b, fn):
    """

    Returns the difference between two list, after applying the provided function to each list element of both.

    Create a set by applying fn to each element in b, then use list comprehension in combination with fn on a to only keep
    values not contained in the previously created set.

    >>> from math import floor
    >>> difference_by([2.1, 1.2], [2.3, 3.4],floor)
    [1.2]

    >>> difference_by([{ 'x': 2 }, { 'x': 1 }], [{ 'x': 1 }], lambda v : v['x'])
    [{'x': 2}]
    """
    b = set(map(fn, b))
    return [item for item in a if fn(item) not in b]


def has_duplicates(lst):
    """
    Checks a flat list for duplicate values. Returns True if duplicate values exist and False if values are all unique.

    This function compares the length of the list with length of the set() of the list. set() removes duplicate values
    from the list.

    >>> has_duplicates([1,2,3,4,5,5])
    True

    >>> has_duplicates([1,2,3,4,5])
    False
    """
    return len(lst) != len(set(lst))


def insertion_sort(lst):
    """
    On a very basic level, an insertion sort algorithm contains the logic of shifting around and inserting elements in
    order to sort an unordered list of any size. The way that it goes about inserting elements, however, is what makes
    insertion sort so very interesting!
    >>> insertion_sort([7,4,9,2,6,3])
    [2, 3, 4, 6, 7, 9]
    """
    for i in range(1, len(lst)):
        key = lst[i]
        j = i - 1
        while j >= 0 and key < lst[j]:
            lst[j + 1] = lst[j]
            j -= 1
            lst[j + 1] = key
    return lst


def shuffle(lst):
    """
    Randomizes the order of the values of an list, returning a new list.

    Uses the Fisher-Yates algorithm to reorder the elements of the list.

    >>> foo = [1,2,3]
    >>> bar = shuffle(foo)
    >>> foo == bar
    False

    note:: The same algorithm is already implemented via random.shuffle.
    """
    temp_lst = deepcopy(lst)
    m = len(temp_lst)
    while m:
        m -= 1
        i = randint(0, m)
        temp_lst[m], temp_lst[i] = temp_lst[i], temp_lst[m]
    return temp_lst


def zip(*args, fill_value=None):
    """
    Creates a list of elements, grouped based on the position in the original lists.

    Use max combined with list comprehension to get the length of the longest list in the arguments. Loops for
    max_length times grouping elements. If lengths of lists vary fill_value is used. By default fill_value is None.

    >>> zip(['a', 'b'], [1, 2], [True, False])
    [['a', 1, True], ['b', 2, False]]

    >>> zip(['a'], [1, 2], [True, False])
    [['a', 1, True], [None, 2, False]]

    >>> zip(['a'], [1, 2], [True, False], fill_value = '_')
    [['a', 1, True], ['_', 2, False]]

    note:: Already implemented via itertools.zip_longest()
    """
    max_length = max([len(lst) for lst in args])
    result = []
    for i in range(max_length):
        result.append([
            args[k][i] if i < len(args[k]) else fill_value for k in range(len(args))
        ])
    return result


# Math


def average(*args):
    """
    Returns the average of two or more numbers.

    Takes the sum of all the args and divides it by len(args). The second argument 0.0 in sum is to handle floating
    point division in python3.

    >>> average(*[1, 2, 3])
    2.0
    >>> average(1, 2, 3)
    2.0

    note:: Already implemented via statistics.mean. statistics.mean takes an array as an argument whereas this function
    takes variadic arguments.
    """
    return sum(args, 0.0) / len(args)


def factorial(num):
    """
    Calculates the factorial of a number.

    Use recursion. If num is less than or equal to 1, return 1. Otherwise, return the product of num and the factorial
    of num - 1. Throws an exception if num is a negative or a floating point number.

    >>> factorial(6)
    720
    """
    if not ((num >= 0) & (num % 1 == 0)):
        raise Exception(
            f"Number( {num} ) can't be floating point or negative ")
    return 1 if num == 0 else num * factorial(num - 1)


def gcd(*args):
    """
    Calculates the greatest common divisor between two or more numbers/lists.

    The helperGcdfunction uses recursion. Base case is when y equals 0. In this case, return x. Otherwise, return the
    GCD of y and the remainder of the division x/y.

    Uses the reduce function from the inbuilt module functools. Also defines a method spread for javascript like
    spreading of lists.

    >>> gcd(8,36)
    4
    """
    numbers = []
    numbers.extend(spread(list(args)))

    def _gcd(x, y):
        return x if not y else gcd(y, x % y)

    return reduce((lambda x, y: _gcd(x, y)), numbers)


def lcm(*args):
    """
    Returns the least common multiple of two or more numbers.

    Use the greatest common divisor (GCD) formula and the fact that lcm(x,y) = x * y / gcd(x,y) to determine the least
    common multiple. The GCD formula uses recursion.

    Uses reduce function from the inbuilt module functools. Also defines a method spread for javascript like spreading
    of lists.

    >>> lcm(12, 7)
    84.0

    >>> lcm([1, 3, 4], 5)
    60.0
    """
    numbers = []
    numbers.extend(spread(list(args)))

    def _gcd(x, y):
        return x if not y else _gcd(y, x % y)

    def _lcm(x, y):
        return x * y / _gcd(x, y)

    return reduce((lambda x, y: _lcm(x, y)), numbers)


def max_n(lst, n=1, reverse=True):
    """
    Returns the n maximum elements from the provided list. If n is greater than or equal to the provided list's length,
    then return the original list(sorted in descending order).

    Use list.sort() combined with the deepcopy function from the inbuilt copy module to create a shallow clone of the
    list and sort it in ascending order and then use list.reverse() reverse it to make it descending order. Use [:n] to
    get the specified number of elements. Omit the second argument, n, to get a one-element list.

    >>> max_n([1, 2, 3])
    [3]
    >>> max_n([1, 2, 3], 2)
    [3, 2]
    """
    return sorted(lst, reverse=reverse)[:n]


def min_n(lst, n=1):
    """
    Returns the n minimum elements from the provided list. If n is greater than or equal to the provided list's length,
    then return the original list(sorted in ascending order).

    Use list.sort() combined with the deepcopy function from the inbuilt copy module to create a shallow clone of the
    list and sort it in ascending order. Use [:n] to get the specified number of elements. Omit the second argument, n,
    to get a one-element list.

    >>> min_n([1, 2, 3])
    [1]

    >>> min_n([1, 2, 3], 2)
    [1, 2]
    """
    numbers = deepcopy(lst)
    numbers.sort()
    return numbers[:n]


# Object


def all_unique(lst):
    """
    Checks a flat list for all unique values. Returns True if list values are all unique and False if list values aren't
    all unique.

    This function compares the length of the list with length of the set() of the list. set() removes duplicate values
    from the list.

    >>> all_unique([1,2,3,4,5,6])
    True

    >>> all_unique([1,2,2,3,4,5])
    False
    """
    return len(lst) == len(set(lst))


def keys_only(flat_dict):
    """
    Function which accepts a dictionary of key value pairs and returns a new flat list of only the keys.

    Uses the .items() function with a for loop on the dictionary to track both the key and value and returns a new list
    by appending the keys to it. Best used on 1 level-deep key:value pair dictionaries (a flat dictionary) and not
    nested data-structures which are also commonly used with dictionaries. (a flat dictionary resembles a json and a
    flat list an array for javascript people).

    >>> ages = {
    ...     "Peter": 10,
    ...     "Isabel": 11,
    ...     "Anna": 9,
    ... }
    >>> keys_only(ages)
    ['Peter', 'Isabel', 'Anna']
    """
    lst = []
    for k, v in flat_dict.items():
        lst.append(k)
    return lst


def values_only(dict):
    """
    Function which accepts a dictionary of key value pairs and returns a new flat list of only the values.

    Uses the .items() function with a for loop on the dictionary to track both the key and value of the iteration and
    returns a new list by appending the values to it. Best used on 1 level-deep key:value pair dictionaries and not
    nested data-structures.

    >>> ages = {
    ...     "Peter": 10,
    ...     "Isabel": 11,
    ...     "Anna": 9,
    ... }
    >>> values_only(ages)
    [10, 11, 9]
    """
    lst = []
    for k, v in dict.items():
        lst.append(v)
    return lst


# String


def byte_size(string):
    """
    Returns the length of a string in bytes.

    utf-8 encodes a given string and find its length.

    >>> byte_size('😀')
    4
    >>> byte_size('Hello World')
    11
    """
    return len(string.encode('utf-8'))


def capitalize(string, lower_rest=False):
    """
    Capitalizes the first letter of a string.

    Capitalizes the fist letter of the sring and then adds it with rest of the string. Omit the lower_rest parameter to
    keep the rest of the string intact, or set it to true to convert to lowercase.


    >>> capitalize('fooBar')
    'FooBar'
    >>> capitalize('fooBar', True)
    'Foobar'
    """
    return string[:1].upper() + (string[1:].lower() if lower_rest else string[1:])


def capitalize_every_word(string):
    """
    Capitalizes the first letter of every word in a string.

    Uses str.title to capitalize first letter of every word in the string.

    capitalize_every_word('hello world!')
    'Hello World!'
    """
    return string.title()


def count_vowels(str):
    """
    Retuns number of vowels in provided string.

    Use a regular expression to count the number of vowels (A, E, I, O, U) in a string.

    >>> count_vowels('foobar')
    3
    >>> count_vowels('gym')
    0
    """
    return len(re.findall(r'[aeiou]', str, re.IGNORECASE))


def decapitalize(string, upper_rest=False):
    """
    Decapitalizes the first letter of a string.

    Decapitalizes the fist letter of the sring and then adds it with rest of the string. Omit the upper_rest parameter
    to keep the rest of the string intact, or set it to true to convert to uppercase.

    >>> decapitalize('FooBar')
    'fooBar'

    >>> decapitalize('FooBar', True)
    'fOOBAR'
    """
    return string[:1].lower() + (string[1:].upper() if upper_rest else string[1:])


def is_lower_case(string):
    """
    Checks if a string is lower case.

    Convert the given string to lower case, using str.lower() method and compare it to the original.

    >>> is_lower_case('abc')
    True

    >>> is_lower_case('a3@$')
    True

    >>> is_lower_case('Ab4')
    False
    """
    return string == string.lower()


def is_upper_case(string):
    """
    Checks if a string is upper case.

    Convert the given string to upper case, using str.upper() method and compare it to the original.

    >>> is_upper_case('ABC')
    True

    >>> is_upper_case('a3@$')
    False

    >>> is_upper_case('aB4')
    False
    """
    return string == string.upper()


def palindrome(string):
    """
    Returns True if the given string is a palindrome, False otherwise.

    Convert string str.lower() and use re.sub to remove non-alphanumeric characters from it. Then compare the new string
    to the reversed.

    >>> palindrome('taco cat')
    True
    """
    from re import sub
    s = sub('[\W_]', '', string.lower())
    return s == s[::-1]


if __name__ == "__main__":
    import doctest

    doctest.testmod()
